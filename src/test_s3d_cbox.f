! Copyright (C) |Meso|Star> 2015 (contact@meso-star.com)
!
! This software is a computer program whose purpose is to describe a
! virtual 3D environment that can be ray-traced and sampled both robustly
! and efficiently.
!
!
! This software is governed by the CeCILL license under French law and
! abiding by the rules of distribution of free software. You can use,
! modify and/or redistribute the software under the terms of the CeCILL
! license as circulated by CEA, CNRS and INRIA at the following URL
! "http://www.cecill.info".
!
! As a counterpart to the access to the source code and rights to copy,
! modify and redistribute granted by the license, users are provided only
! with a limited warranty and the software's author, the holder of the
! economic rights, and the successive licensors have only limited
! liability.
!
! In this respect, the user's attention is drawn to the risks associated
! with loading, using, modifying and/or developing or reproducing the
! software by the user in light of its specific status of free software,
! that may mean that it is complicated to manipulate, and that also
! therefore means that it is reserved for developers and experienced
! professionals having in-depth computer knowledge. Users are therefore
! encouraged to load and test the software's suitability as regards their
! requirements in conditions enabling the security of their systems and/or
! data to be ensured and, more generally, to use and operate it in the
! same conditions as regards security.
!
! The fact that you are presently reading this means that you have had
! knowledge of the CeCILL license and that you accept its terms.

MODULE TEST_S3D_CBOX

! (1): Special warning about some API calls manipulating unsigned C-side
! ---------------------------------------------------------------------
! As unsigned types do not exist Fortran-side, SSP binding makes use of C_INT
! where C_UINT (that doesn't exist) would have been the perfect match
! Doing this garantees the size matches, but exposes to overflows if used
! careless Fortran-side (a 'too big' uint will be treated as negative)
! A workaround is to TRANFER the bytes of these unsigned-stored-in-a-signed
! to int64 (see below)

USE ISO_C_BINDING, ONLY: C_PTR, C_INT, C_FLOAT
IMPLICIT NONE

TYPE, BIND(C) :: CBOX_DESC
  TYPE(C_PTR) :: VERTICES
  TYPE(C_PTR) :: INDICES
END TYPE CBOX_DESC

!*******************************************************************************
! Box
!*******************************************************************************
REAL(C_FLOAT), BIND(C, NAME='cbox_walls'), TARGET :: CBOX_WALLS(24)
INTEGER(C_INT), BIND(C, NAME='cbox_walls_ids'), TARGET :: CBOX_WALLS_IDS(30)
REAL(C_FLOAT), BIND(C, NAME='cbox_short_block'), TARGET :: CBOX_SHORT_BLOCK(24)
REAL(C_FLOAT), BIND(C, NAME='cbox_tall_block'), TARGET :: CBOX_TALL_BLOCK(24)
INTEGER(C_INT), BIND(C, NAME='cbox_block_ids'), TARGET :: CBOX_BLOCK_IDS(30)

INTEGER(C_INT), BIND(C, NAME='cbox_walls_nverts') :: CBOX_WALLS_NVERTS
INTEGER(C_INT), BIND(C, NAME='cbox_walls_ntris') :: CBOX_WALLS_NTRIS
INTEGER(C_INT), BIND(C, NAME='cbox_block_nverts') :: CBOX_BLOCK_NVERTS
INTEGER(C_INT), BIND(C, NAME='cbox_block_ntris') :: CBOX_BLOCK_NTRIS

TYPE(CBOX_DESC), BIND(C, NAME='cbox_walls_desc'), TARGET :: CBOX_WALLS_DESC

INTERFACE

!*******************************************************************************
! Callbacks
!*******************************************************************************

SUBROUTINE CBOX_GET_IDS(ITRI, IDS, DATA) BIND(C, NAME="cbox_get_ids")
  USE ISO_C_BINDING, ONLY: C_INT, C_PTR
  IMPLICIT NONE
  INTEGER(C_INT), INTENT(IN), VALUE :: ITRI ! See (1)
  INTEGER(C_INT), INTENT(IN) :: IDS(3) ! See (1)
  TYPE(C_PTR), INTENT(IN) :: DATA
END SUBROUTINE CBOX_GET_IDS

SUBROUTINE CBOX_GET_POSITION(ITRI, POSITION, DATA) BIND(C, NAME="cbox_get_position")
  USE ISO_C_BINDING, ONLY: C_INT, C_FLOAT, C_PTR
  IMPLICIT NONE
  INTEGER(C_INT), INTENT(IN), VALUE :: ITRI ! See (1)
  REAL(C_FLOAT), INTENT(IN) :: POSITION(3)
  TYPE(C_PTR), INTENT(IN) :: DATA
END SUBROUTINE CBOX_GET_POSITION

SUBROUTINE CBOX_GET_NORMAL(ITRI, NORMAL, DATA) BIND(C, NAME="cbox_get_normal")
  USE ISO_C_BINDING, ONLY: C_INT, C_FLOAT, C_PTR
  IMPLICIT NONE
  INTEGER(C_INT), INTENT(IN), VALUE :: ITRI ! See (1)
  REAL(C_FLOAT), INTENT(IN) :: NORMAL(3)
  TYPE(C_PTR), INTENT(IN) :: DATA
END SUBROUTINE CBOX_GET_NORMAL

SUBROUTINE CBOX_GET_UV(ITRI, UV, DATA) BIND(C, NAME="cbox_get_uv")
  USE ISO_C_BINDING, ONLY: C_INT, C_FLOAT, C_PTR
  IMPLICIT NONE
  INTEGER(C_INT), INTENT(IN), VALUE :: ITRI ! See (1)
  REAL(C_FLOAT), INTENT(IN) :: UV(2)
  TYPE(C_PTR), INTENT(IN) :: DATA
END SUBROUTINE CBOX_GET_UV

END INTERFACE

END MODULE TEST_S3D_CBOX

