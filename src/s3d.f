! Copyright (C) |Meso|Star> 2015 (contact@meso-star.com)
!
! This software is a collection of files whose purpose is to give access
! to the Star-3D library from Fortran programs
!
! This software is governed by the CeCILL license under French law and
! abiding by the rules of distribution of free software. You can use,
! modify and/or redistribute the software under the terms of the CeCILL
! license as circulated by CEA, CNRS and INRIA at the following URL
! "http://www.cecill.info".
!
! As a counterpart to the access to the source code and rights to copy,
! modify and redistribute granted by the license, users are provided only
! with a limited warranty and the software's author, the holder of the
! economic rights, and the successive licensors have only limited
! liability.
!
! In this respect, the user's attention is drawn to the risks associated
! with loading, using, modifying and/or developing or reproducing the
! software by the user in light of its specific status of free software,
! that may mean that it is complicated to manipulate, and that also
! therefore means that it is reserved for developers and experienced
! professionals having in-depth computer knowledge. Users are therefore
! encouraged to load and test the software's suitability as regards their
! requirements in conditions enabling the security of their systems and/or
! data to be ensured and, more generally, to use and operate it in the
! same conditions as regards security.
!
! The fact that you are presently reading this means that you have had
! knowledge of the CeCILL license and that you accept its terms.

#include <rsys/rsys.inc>

MODULE S3D_BINDING

! (1): Special warning about some API calls manipulating unsigned C-side
! ---------------------------------------------------------------------
! As unsigned types do not exist Fortran-side, SSP binding makes use of C_INT
! where C_UINT (that doesn't exist) would have been the perfect match
! Doing this garantees the size matches, but exposes to overflows if used
! careless Fortran-side (a 'too big' uint will be treated as negative)
! A workaround is to TRANFER the bytes of these unsigned-stored-in-a-signed
! to int64 (see below)

USE ISO_C_BINDING, ONLY: C_FUNPTR, C_PTR, C_INT, C_FLOAT
IMPLICIT NONE

! s3d_rays_flag
ENUM, BIND(C)
  ENUMERATOR :: S3D_RAYS_SINGLE_ORIGIN = 1
  ENUMERATOR :: S3D_RAYS_SINGLE_DIRECTION = 2
  ENUMERATOR :: S3D_RAYS_SINGLE_RANGE = 4
END ENUM

! s3d_attrib_usage (Attributes of a shape)
ENUM, BIND(C)
  ENUMERATOR :: S3D_POSITION
  ENUMERATOR :: S3D_ATTRIB_0
  ENUMERATOR :: S3D_ATTRIB_1
  ENUMERATOR :: S3D_ATTRIB_2
  ENUMERATOR :: S3D_ATTRIB_3
  ENUMERATOR :: S3D_ATTRIBS_COUNT__
  ! Unormalized world space face normal. For triangular meshes, the outward
  ! orientation is defined with respect to the Clock Wise vertex ordering
  ENUMERATOR :: S3D_GEOMETRY_NORMAL
END ENUM

! s3d_type
ENUM, BIND(C)
  ENUMERATOR :: S3D_FLOAT
  ENUMERATOR :: S3D_FLOAT2
  ENUMERATOR :: S3D_FLOAT3
  ENUMERATOR :: S3D_FLOAT4
END ENUM

! s3d_transform_space
ENUM, BIND(C)
  ENUMERATOR :: S3D_LOCAL_TRANSFORM ! The transformation is local to the shape space
  ENUMERATOR :: S3D_WORLD_TRANSFORM ! The transformation is expressed in world space
END ENUM

! Primitive descriptor. The <geom|inst> indentifiers cover a compact ranges of
! value. They can be used in conjunction with a dynamic array to map from s3d
! geometry to application geometry
 TYPE, BIND(C) :: S3D_PRIMITIVE
  INTEGER(C_INT) :: prim_id ! Primitive identifier; See (1)
  INTEGER(C_INT) :: geom_id ! Geometry identifier; See (1)
  INTEGER(C_INT) :: inst_id ! Instance identifier; See (1)
  INTEGER(C_INT) :: scene_prim_id ! Identifier of the primitive in the scene; See (1)
  ! Internal data. Should not be accessed
  TYPE(C_PTR) :: mesh__
  TYPE(C_PTR) :: inst__
 END TYPE S3D_PRIMITIVE

! Untyped vertex attribute
 TYPE, BIND(C) :: S3D_ATTRIB
  REAL(C_FLOAT) :: value(4)
  INTEGER(C_INT) :: type
  INTEGER(C_INT) :: usage
 END TYPE S3D_ATTRIB

! Describe a per vertex data
 TYPE, BIND(C) :: S3D_VERTEX_DATA
  ! Semantic of the data. Note that the S3D_GEOMETRY_NORMAL is not a valid
  ! vertex usage
  INTEGER(C_INT) :: usage
  INTEGER(C_INT) :: type
  ! Retreive the vertex data value of `ivert'. Set it to S3D_KEEP, to keep the
  ! previously set data
  TYPE(C_FUNPTR) :: GET
 END TYPE S3D_VERTEX_DATA

! Intersection point
 TYPE, BIND(C) :: S3D_HIT
  TYPE(S3D_PRIMITIVE) :: prim ! Intersected primitive
  REAL(C_FLOAT) :: normal(3) ! Unnormalized geometry normal
  REAL(C_FLOAT) :: uv(2) ! Barycentric coordinates of the hit onto `prim'
  REAL(C_FLOAT) :: distance ! Hit distance from the ray origin
 END TYPE S3D_HIT

! s3d_session_flag
ENUM, BIND(C)
  ENUMERATOR :: S3D_TRACE = 1
  ENUMERATOR :: S3D_SAMPLE = 2
  ENUMERATOR :: S3D_GET_PRIMITIVE = 4
END ENUM

! All the s3d structures are ref counted. Once created with the appropriated
! `s3d_<TYPE>_create' function, the caller implicitly owns the created data,
! i.e. its reference counter is set to 1. The s3d_<TYPE>_ref_<get|put>
! functions get or release a reference on the data, i.e. they increment or
! decrement the reference counter, respectively. When this counter reach 0 the
! object is silently destroyed and cannot be used anymore.

! Global C variables

TYPE(S3D_PRIMITIVE), BIND(C, NAME='S3D_PRIMITIVE_NULL'), TARGET :: S3D_PRIMITIVE_NULL

INTERFACE

!*******************************************************************************
! Device API - A device is the entry point of the s3d library. Applications
! use a s3d_device to create others s3d resources.
! ******************************************************************************
 FUNCTION s3d_device_create(logger, allocator, verbose, dev) BIND(C, NAME='s3d_device_create')
  USE ISO_C_BINDING, ONLY: C_INT, C_PTR
  IMPLICIT NONE
  TYPE(C_PTR), INTENT(IN), VALUE :: logger ! May be NULL <=> use default logger
  TYPE(C_PTR), INTENT(IN), VALUE :: allocator ! May be NULL <=> use default allocator
  INTEGER(C_INT), INTENT(IN), VALUE :: verbose ! Define the level of verbosity
  TYPE(C_PTR), INTENT(OUT) :: dev
  INTEGER(C_INT) :: s3d_device_create
 END FUNCTION s3d_device_create

 FUNCTION s3d_device_ref_get(dev) BIND(C, NAME='s3d_device_ref_get')
  USE ISO_C_BINDING, ONLY: C_INT, C_PTR
  IMPLICIT NONE
  TYPE(C_PTR), INTENT(IN), VALUE :: dev
  INTEGER(C_INT) :: s3d_device_ref_get
 END FUNCTION s3d_device_ref_get

 FUNCTION s3d_device_ref_put(dev) BIND(C, NAME='s3d_device_ref_put')
  USE ISO_C_BINDING, ONLY: C_INT, C_PTR
  IMPLICIT NONE
  TYPE(C_PTR), INTENT(IN), VALUE :: dev
  INTEGER(C_INT) :: s3d_device_ref_put
 END FUNCTION s3d_device_ref_put

!*******************************************************************************
! Scene API - A scene is a collection of untyped shapes. It can be ray-traced
! and/or "instantiated" through a shape.
! ******************************************************************************
 FUNCTION s3d_scene_create(dev, scn) BIND(C, NAME='s3d_scene_create')
  USE ISO_C_BINDING, ONLY: C_INT, C_PTR
  IMPLICIT NONE
  TYPE(C_PTR), INTENT(IN), VALUE :: dev
  TYPE(C_PTR), INTENT(OUT) :: scn
  INTEGER(C_INT) :: s3d_scene_create
 END FUNCTION s3d_scene_create

 FUNCTION s3d_scene_ref_get(scn) BIND(C, NAME='s3d_scene_ref_get')
  USE ISO_C_BINDING, ONLY: C_INT, C_PTR
  IMPLICIT NONE
  TYPE(C_PTR), INTENT(IN), VALUE :: scn
  INTEGER(C_INT) :: s3d_scene_ref_get
 END FUNCTION s3d_scene_ref_get

 FUNCTION s3d_scene_ref_put(scn) BIND(C, NAME='s3d_scene_ref_put')
  USE ISO_C_BINDING, ONLY: C_INT, C_PTR
  IMPLICIT NONE
  TYPE(C_PTR), INTENT(IN), VALUE :: scn
  INTEGER(C_INT) :: s3d_scene_ref_put
 END FUNCTION s3d_scene_ref_put

 FUNCTION s3d_scene_instantiate(scn, shape) BIND(C, NAME='s3d_scene_instantiate')
  USE ISO_C_BINDING, ONLY: C_INT, C_PTR
  IMPLICIT NONE
  TYPE(C_PTR), INTENT(IN), VALUE :: scn
  TYPE(C_PTR), INTENT(OUT) :: shape
  INTEGER(C_INT) :: s3d_scene_instantiate
 END FUNCTION s3d_scene_instantiate

! Attach the shape to the scene. If the shape is already attached to the same
! or another scene, nothing is performed and a RES_BAD_ARG error is returned.
! On success, the scene gets a reference onto the attached shape
 FUNCTION s3d_scene_attach_shape(scn, shape) BIND(C, NAME='s3d_scene_attach_shape')
  USE ISO_C_BINDING, ONLY: C_INT, C_PTR
  IMPLICIT NONE
  TYPE(C_PTR), INTENT(IN), VALUE :: scn
  TYPE(C_PTR), INTENT(IN), VALUE :: shape
  INTEGER(C_INT) :: s3d_scene_attach_shape
 END FUNCTION s3d_scene_attach_shape

! Remove the shape from the scene. After its detachment, the scene
! release its reference on the shape
 FUNCTION s3d_scene_detach_shape(scn, shape) BIND(C, NAME='s3d_scene_detach_shape')
  USE ISO_C_BINDING, ONLY: C_INT, C_PTR
  IMPLICIT NONE
  TYPE(C_PTR), INTENT(IN), VALUE :: scn
  TYPE(C_PTR), INTENT(IN), VALUE :: shape
  INTEGER(C_INT) :: s3d_scene_detach_shape
 END FUNCTION s3d_scene_detach_shape

! Detach all the shapes from the scene and release the reference that the
! scene takes onto them
 FUNCTION s3d_scene_clear(scn) BIND(C, NAME='s3d_scene_clear')
  USE ISO_C_BINDING, ONLY: C_INT, C_PTR
  IMPLICIT NONE
  TYPE(C_PTR), INTENT(IN), VALUE :: scn
  INTEGER(C_INT) :: s3d_scene_clear
 END FUNCTION s3d_scene_clear

! Synchronize the scene geometry with the geometry of its attached shapes. If
! a s3d_scene_begin_session is already active on `scn' or one of its attached
! instance a RES_BAD_OP error is returned. On success neither another begin
! session nor a clear or shape_detach can be invoked on `scn' and its attached
! instances until s3d_scene_end_session is called.
 FUNCTION s3d_scene_begin_session(scn, session_mask) BIND(C, NAME='s3d_scene_begin_session')
  USE ISO_C_BINDING, ONLY: C_INT, C_PTR
  IMPLICIT NONE
  TYPE(C_PTR), INTENT(IN), VALUE :: scn
  INTEGER(C_INT), INTENT(IN), VALUE :: session_mask ! Combination of s3d_session_flag
  INTEGER(C_INT) :: s3d_scene_begin_session
 END FUNCTION s3d_scene_begin_session

! End the session on the `scn'
 FUNCTION s3d_scene_end_session(scn) BIND(C, NAME='s3d_scene_end_session')
  USE ISO_C_BINDING, ONLY: C_INT, C_PTR
  IMPLICIT NONE
  TYPE(C_PTR), INTENT(IN), VALUE :: scn
  INTEGER(C_INT) :: s3d_scene_end_session
 END FUNCTION s3d_scene_end_session

 FUNCTION s3d_scene_get_session_mask(scn, mask) BIND(C, NAME='s3d_scene_get_session_mask')
  USE ISO_C_BINDING, ONLY: C_INT, C_PTR
  IMPLICIT NONE
  TYPE(C_PTR), INTENT(IN), VALUE :: scn
  INTEGER(C_INT), INTENT(OUT) :: mask
  INTEGER(C_INT) :: s3d_scene_get_session_mask
 END FUNCTION s3d_scene_get_session_mask

! Trace a ray into the `scn' and return the closest intersection. The ray is
! defined by `origin' + t*`direction' = 0 with t in [`range[0]', `range[1]').
! Note that if range is degenerated (i.e. `range[0]' >= `range[1]') then the
! ray is not traced and `hit' is set to S3D_HIT_NULL. Can be called only if an
! S3D_TRACE session is active on `scn'
 FUNCTION s3d_scene_trace_ray(scn, origin, direction, range, hit) BIND(C, NAME='s3d_scene_trace_ray')
  USE ISO_C_BINDING, ONLY: C_INT, C_PTR, C_FLOAT
  IMPORT S3D_HIT
  IMPLICIT NONE
  TYPE(C_PTR), INTENT(IN), VALUE :: scn
  REAL(C_FLOAT), INTENT(IN) :: origin(3)
  REAL(C_FLOAT), INTENT(IN) :: direction(3)
  REAL(C_FLOAT), INTENT(IN) :: range(2)
  TYPE(S3D_HIT), INTENT(OUT) :: hit
  INTEGER(C_INT) :: s3d_scene_trace_ray
 END FUNCTION s3d_scene_trace_ray

! Trace a bundle of rays into `scn' and return the closest intersection along
! them. The rays are defined by `origin' + t*`direction' = 0 with t in
! [`range[0]', `range[1]').  Note that if a range is degenerated (i.e.
! `range[0]' >= `range[1]') then its associated ray is not traced and `hit' is
! set to S3D_HIT_NULL. Can be called only if an S3D_TRACE session is active on
! `scn'
 FUNCTION s3d_scene_trace_rays(scn, nrays, mask, origins, directions, ranges, hits) BIND(C, NAME='s3d_scene_trace_rays')
  USE ISO_C_BINDING, ONLY: C_INT, C_PTR, C_FLOAT
  IMPORT S3D_HIT
  IMPLICIT NONE
  TYPE(C_PTR), INTENT(IN), VALUE :: scn
  INTEGER(C_INT), INTENT(IN), VALUE :: nrays
  INTEGER(C_INT), INTENT(IN), VALUE :: mask
  REAL(C_FLOAT), INTENT(IN) :: origins(*)
  REAL(C_FLOAT), INTENT(IN) :: directions(*)
  REAL(C_FLOAT), INTENT(IN) :: ranges(*)
  TYPE(S3D_HIT), INTENT(OUT) :: hits(*)
  INTEGER(C_INT) :: s3d_scene_trace_rays
 END FUNCTION s3d_scene_trace_rays

! Uniformly sample the scene and returned the sampled primitive and its sample
! uv position. Can be called only if an S3D_SAMPLE session is active on `scn'
 FUNCTION s3d_scene_sample(scn, u, v, w, primitive, st) BIND(C, NAME='s3d_scene_sample')
  USE ISO_C_BINDING, ONLY: C_INT, C_PTR, C_FLOAT
  IMPORT S3D_PRIMITIVE
  IMPLICIT NONE
  TYPE(C_PTR), INTENT(IN), VALUE :: scn
  REAL(C_FLOAT), INTENT(IN), VALUE :: u, v, w ! Random numbers in [0, 1) 
  TYPE(S3D_PRIMITIVE), INTENT(OUT) :: primitive
  REAL(C_FLOAT), INTENT(OUT) :: st(2)
  INTEGER(C_INT) :: s3d_scene_sample
 END FUNCTION s3d_scene_sample

! WARNING: don't use s3d_scene_get_primitive_0 but s3d_scene_get_primitive
!
! Retrieve a primitive from the scene. Can be called only if a
! S3D_GET_PRIMITIVE session is active on `scn'
! Warning: due to differencies in arrays indexing we introduce s3d_scene_get_primitive
! as a true Fortran function using this s3d_scene_get_primitive_
! s3d_scene_get_primitive as the 'indexes start at 1' good property
 FUNCTION s3d_scene_get_primitive_0(scn, iprim, prim) BIND(C, NAME='s3d_scene_get_primitive')
  USE ISO_C_BINDING, ONLY: C_INT, C_PTR
  IMPORT S3D_PRIMITIVE
  IMPLICIT NONE
  TYPE(C_PTR), INTENT(IN), VALUE :: scn
  INTEGER(C_INT), INTENT(IN), VALUE :: iprim ! in [0, #prims); See (1)
  TYPE(S3D_PRIMITIVE), INTENT(OUT) :: prim
  INTEGER(C_INT) :: s3d_scene_get_primitive_0
 END FUNCTION s3d_scene_get_primitive_0

! Retrieve the number of scene primitives. Can be called only if a sessio is
! active on `scn'
 FUNCTION s3d_scene_primitives_count(scn, primitives_count) BIND(C, NAME='s3d_scene_primitives_count')
  USE ISO_C_BINDING, ONLY: C_INT, C_PTR, C_SIZE_T
  IMPLICIT NONE
  TYPE(C_PTR), INTENT(IN), VALUE :: scn
  INTEGER(C_SIZE_T), INTENT(OUT) :: primitives_count
  INTEGER(C_INT) :: s3d_scene_primitives_count
 END FUNCTION s3d_scene_primitives_count

! Compute the overall scene surface area. Can be called only if a session is
! active on `scn'
 FUNCTION s3d_scene_compute_area(scn, area) BIND(C, NAME='s3d_scene_compute_area')
  USE ISO_C_BINDING, ONLY: C_INT, C_PTR, C_FLOAT
  IMPLICIT NONE
  TYPE(C_PTR), INTENT(IN), VALUE :: scn
  REAL(C_FLOAT), INTENT(OUT) :: area
  INTEGER(C_INT) :: s3d_scene_compute_area
 END FUNCTION s3d_scene_compute_area

! This function assumes that the scene defines a closed volume and that the
! normals point into the volume. Can be called only if a session is active on
! `scn'
 FUNCTION s3d_scene_compute_volume(scn, volume) BIND(C, NAME='s3d_scene_compute_volume')
  USE ISO_C_BINDING, ONLY: C_INT, C_PTR, C_FLOAT
  IMPLICIT NONE
  TYPE(C_PTR), INTENT(IN), VALUE :: scn
  REAL(C_FLOAT), INTENT(OUT) :: volume
  INTEGER(C_INT) :: s3d_scene_compute_volume
 END FUNCTION s3d_scene_compute_volume

! Retrieve the Axis Aligned Bounding Box of the scene. Can be called only if a
! session is active on `scn'
 FUNCTION s3d_scene_get_aabb(scn, lower, upper) BIND(C, NAME='s3d_scene_get_aabb')
  USE ISO_C_BINDING, ONLY: C_INT, C_PTR, C_FLOAT
  IMPLICIT NONE
  TYPE(C_PTR), INTENT(IN), VALUE :: scn
  REAL(C_FLOAT), INTENT(OUT) :: lower(3) ! AABB lower bound
  REAL(C_FLOAT), INTENT(OUT) :: upper(3) ! AABB upper bound
  INTEGER(C_INT) :: s3d_scene_get_aabb
 END FUNCTION s3d_scene_get_aabb

! Retrieve the device from which the scene was created
 FUNCTION s3d_scene_get_device(scn, dev) BIND(C, NAME='s3d_scene_get_device')
  USE ISO_C_BINDING, ONLY: C_INT, C_PTR
  IMPLICIT NONE
  TYPE(C_PTR), INTENT(IN), VALUE :: scn
  TYPE(C_PTR), INTENT(OUT) :: dev
  INTEGER(C_INT) :: s3d_scene_get_device
 END FUNCTION s3d_scene_get_device

!*******************************************************************************
! Shape API - A shape defines a geometry that can be attached to *one* scene.
!*******************************************************************************
 FUNCTION s3d_shape_create_mesh(dev, shape) BIND(C, NAME='s3d_shape_create_mesh')
  USE ISO_C_BINDING, ONLY: C_INT, C_PTR
  IMPLICIT NONE
  TYPE(C_PTR), INTENT(IN), VALUE :: dev
  TYPE(C_PTR), INTENT(OUT) :: shape
  INTEGER(C_INT) :: s3d_shape_create_mesh
 END FUNCTION s3d_shape_create_mesh

 FUNCTION s3d_shape_ref_get(shape) BIND(C, NAME='s3d_shape_ref_get')
  USE ISO_C_BINDING, ONLY: C_INT, C_PTR
  IMPLICIT NONE
  TYPE(C_PTR), INTENT(IN), VALUE :: shape
  INTEGER(C_INT) :: s3d_shape_ref_get
 END FUNCTION s3d_shape_ref_get

 FUNCTION s3d_shape_ref_put(shape) BIND(C, NAME='s3d_shape_ref_put')
  USE ISO_C_BINDING, ONLY: C_INT, C_PTR
  IMPLICIT NONE
  TYPE(C_PTR), INTENT(IN), VALUE :: shape
  INTEGER(C_INT) :: s3d_shape_ref_put
 END FUNCTION s3d_shape_ref_put

! Retrieve the id of the shape. This id covers a compact range of value.
! Consequently, it can be used to map from the s3d shapes to the geometry
! representation of the caller with a simple dynamic array
 FUNCTION s3d_shape_get_id(shape, id) BIND(C, NAME='s3d_shape_get_id')
  USE ISO_C_BINDING, ONLY: C_INT, C_PTR
  IMPLICIT NONE
  TYPE(C_PTR), INTENT(IN), VALUE :: shape
  INTEGER(C_INT), INTENT(OUT) :: id ! See (1)
  INTEGER(C_INT) :: s3d_shape_get_id
 END FUNCTION s3d_shape_get_id

! Enable/disable the shape, i.e. it cannot be hit when its associated scene is
! ray-traced or sampled
 FUNCTION s3d_shape_enable(shape, enable) BIND(C, NAME='s3d_shape_enable')
  USE ISO_C_BINDING, ONLY: C_INT, C_PTR, C_CHAR
  IMPLICIT NONE
  TYPE(C_PTR), INTENT(IN), VALUE :: shape
  CHARACTER(C_CHAR), INTENT(IN), VALUE :: enable
  INTEGER(C_INT) :: s3d_shape_enable
 END FUNCTION s3d_shape_enable

! Return whether or not the shape is enabled, i.e. ray-traced. Default is 1
 FUNCTION s3d_shape_is_enabled(shape, is_enabled) BIND(C, NAME='s3d_shape_is_enabled')
  USE ISO_C_BINDING, ONLY: C_INT, C_PTR, C_CHAR
  IMPLICIT NONE
  TYPE(C_PTR), INTENT(IN), VALUE :: shape
  CHARACTER(C_CHAR), INTENT(OUT) :: is_enabled
  INTEGER(C_INT) :: s3d_shape_is_enabled
 END FUNCTION s3d_shape_is_enabled

! Define whether the shape is attached or not
 FUNCTION s3d_shape_is_attached(shape, is_attached) BIND(C, NAME='s3d_shape_is_attached')
  USE ISO_C_BINDING, ONLY: C_INT, C_PTR, C_CHAR
  IMPLICIT NONE
  TYPE(C_PTR), INTENT(IN), VALUE :: shape
  CHARACTER(C_CHAR), INTENT(OUT) :: is_attached
  INTEGER(C_INT) :: s3d_shape_is_attached
 END FUNCTION s3d_shape_is_attached

! Flip the surface orientation, i.e. flip the geometric normal of the surface 
 FUNCTION s3d_shape_flip_surface(shape) BIND(C, NAME='s3d_shape_flip_surface')
  USE ISO_C_BINDING, ONLY: C_INT, C_PTR
  IMPLICIT NONE
  TYPE(C_PTR), INTENT(IN), VALUE :: shape
  INTEGER(C_INT) :: s3d_shape_flip_surface
 END FUNCTION s3d_shape_flip_surface

!*******************************************************************************
! Primitive API - Define a geometric primitive of a shape
!*******************************************************************************
! Retrieve the attribute of the shape primitive `iprim' at the barycentric
! coordinates `uv'
 FUNCTION s3d_primitive_get_attrib(prim, attr, st, attrib) BIND(C, NAME='s3d_primitive_get_attrib')
  USE ISO_C_BINDING, ONLY: C_INT, C_FLOAT
  IMPORT S3D_ATTRIB
  IMPORT S3D_PRIMITIVE
  IMPLICIT NONE
  TYPE(S3D_PRIMITIVE), INTENT(IN) :: prim
  INTEGER(C_INT), INTENT(IN), VALUE :: attr ! Attribute to retrieve
  REAL(C_FLOAT), INTENT(IN) :: st(2) ! Parametric coordinates of `attr' on `iprim'
  TYPE(S3D_ATTRIB), INTENT(OUT) :: attrib
  INTEGER(C_INT) :: s3d_primitive_get_attrib
 END FUNCTION s3d_primitive_get_attrib

! Uniform sampling of the primitive
 FUNCTION s3d_primitive_sample(prim, u, v, st) BIND(C, NAME='s3d_primitive_sample')
  USE ISO_C_BINDING, ONLY: C_INT, C_FLOAT
  IMPORT S3D_PRIMITIVE
  IMPLICIT NONE
  TYPE(S3D_PRIMITIVE), INTENT(IN) :: prim
  REAL(C_FLOAT), INTENT(IN) :: u, v ! Random numbers in [0, 1)
  REAL(C_FLOAT), INTENT(OUT) :: st(2) ! Sampled parametric coordinates on prim
  INTEGER(C_INT) :: s3d_primitive_sample
 END FUNCTION s3d_primitive_sample

 FUNCTION s3d_primitive_compute_area(prim, area) BIND(C, NAME='s3d_primitive_compute_area')
  USE ISO_C_BINDING, ONLY: C_INT, C_FLOAT
  IMPORT S3D_PRIMITIVE
  IMPLICIT NONE
  TYPE(S3D_PRIMITIVE), INTENT(IN) :: prim
  REAL(C_FLOAT), INTENT(OUT) :: area
  INTEGER(C_INT) :: s3d_primitive_compute_area
 END FUNCTION s3d_primitive_compute_area

!*******************************************************************************
! Mesh API - Manage a triangular meshes
!*******************************************************************************
! Set/update the data of the indexed triangular meshes
 FUNCTION s3d_mesh_setup_indexed_vertices(shape, ntris, get_indices, nverts, attribs, nattribs, data) BIND(C, NAME='s3d_mesh_setup_indexed_vertices')
  USE ISO_C_BINDING, ONLY: C_INT, C_FLOAT, C_PTR, C_FUNPTR
  IMPORT S3D_VERTEX_DATA
  IMPLICIT NONE
  TYPE(C_PTR), INTENT(IN), VALUE :: shape
  INTEGER(C_INT), INTENT(IN), VALUE :: ntris ! See (1)
  TYPE(C_FUNPTR), INTENT(IN), VALUE :: get_indices ! May be S3D_KEEP, i.e. do not update the indices
  INTEGER(C_INT), INTENT(IN), VALUE :: nverts ! See (1)
  ! List of the shape vertex data. Must have at least an attrib with the
  ! S3D_POSITION usage.
  TYPE(S3D_VERTEX_DATA), INTENT(IN) :: attribs(*)
  INTEGER(C_INT), INTENT(IN), VALUE :: nattribs ! # attributes in the attribs list; See (1)
  TYPE(C_PTR), INTENT(IN), VALUE :: data ! Client data set as the last param of the callbacks
  INTEGER(C_INT) :: s3d_mesh_setup_indexed_vertices
 END FUNCTION s3d_mesh_setup_indexed_vertices

! Copy the mesh data from `src' to `dst'
 FUNCTION s3d_mesh_copy(src, dst) BIND(C, NAME='s3d_mesh_copy')
  USE ISO_C_BINDING, ONLY: C_INT, C_PTR
  IMPORT S3D_VERTEX_DATA
  IMPLICIT NONE
  TYPE(C_PTR), INTENT(IN), VALUE :: src
  TYPE(C_PTR), INTENT(IN), VALUE :: dst
  INTEGER(C_INT) :: s3d_mesh_copy
 END FUNCTION s3d_mesh_copy

 FUNCTION s3d_mesh_get_vertices_count(shape, nverts) BIND(C, NAME='s3d_mesh_get_vertices_count')
  USE ISO_C_BINDING, ONLY: C_INT, C_PTR
  IMPLICIT NONE
  TYPE(C_PTR), INTENT(IN), VALUE :: shape
  INTEGER(C_INT), INTENT(OUT) :: nverts ! See (1)
  INTEGER(C_INT) :: s3d_mesh_get_vertices_count
 END FUNCTION s3d_mesh_get_vertices_count

 FUNCTION s3d_mesh_get_vertex_attrib(shape, ivert, usage, attrib) BIND(C, NAME='s3d_mesh_get_vertex_attrib')
  USE ISO_C_BINDING, ONLY: C_INT, C_PTR
  IMPORT S3D_ATTRIB
  IMPLICIT NONE
  TYPE(C_PTR), INTENT(IN), VALUE :: shape
  INTEGER(C_INT), INTENT(IN), VALUE :: ivert ! See (1)
  INTEGER(C_INT), INTENT(IN), VALUE :: usage
  TYPE(S3D_ATTRIB), INTENT(OUT) :: attrib
  INTEGER(C_INT) :: s3d_mesh_get_vertex_attrib
 END FUNCTION s3d_mesh_get_vertex_attrib

 FUNCTION s3d_mesh_get_triangles_count(shape, ntris) BIND(C, NAME='s3d_mesh_get_triangles_count')
  USE ISO_C_BINDING, ONLY: C_INT, C_PTR
  IMPLICIT NONE
  TYPE(C_PTR), INTENT(IN), VALUE :: shape
  INTEGER(C_INT), INTENT(OUT) :: ntris ! See (1)
  INTEGER(C_INT) :: s3d_mesh_get_triangles_count
 END FUNCTION s3d_mesh_get_triangles_count

 FUNCTION s3d_mesh_get_triangle_indices(shape, itri, ids) BIND(C, NAME='s3d_mesh_get_triangle_indices')
  USE ISO_C_BINDING, ONLY: C_INT, C_PTR
  IMPLICIT NONE
  TYPE(C_PTR), INTENT(IN), VALUE :: shape
  INTEGER(C_INT), INTENT(IN), VALUE :: itri ! See (1)
  INTEGER(C_INT), INTENT(OUT) :: ids(3) ! See (1)
  INTEGER(C_INT) :: s3d_mesh_get_triangle_indices
 END FUNCTION s3d_mesh_get_triangle_indices

!*******************************************************************************
! Instance API - An instance is a shape that encapsulates a scene and that
! supports a local to world transformation. Since the scene geometry is stored
! only a single time even though it is instantiated in several positions, one
! can use this feature to create extremely large scene
!*******************************************************************************
 FUNCTION s3d_instance_set_position(shape, position) BIND(C, NAME='s3d_instance_set_position')
  USE ISO_C_BINDING, ONLY: C_INT, C_FLOAT, C_PTR
  IMPLICIT NONE
  TYPE(C_PTR), INTENT(IN), VALUE :: shape
  REAL(C_FLOAT), INTENT(OUT) :: position(3)
  INTEGER(C_INT) :: s3d_instance_set_position
 END FUNCTION s3d_instance_set_position

 FUNCTION s3d_instance_translate(shape, space, translation) BIND(C, NAME='s3d_instance_translate')
  USE ISO_C_BINDING, ONLY: C_INT, C_FLOAT, C_PTR
  IMPLICIT NONE
  TYPE(C_PTR), INTENT(IN), VALUE :: shape
  INTEGER(C_INT), INTENT(IN), VALUE :: space
  REAL(C_FLOAT), INTENT(OUT) :: translation(3)
  INTEGER(C_INT) :: s3d_instance_translate
 END FUNCTION s3d_instance_translate

END INTERFACE

CONTAINS

! Retrieve a primitive from the scene. Can be called only if a
! S3D_GET_PRIMITIVE session is active on `scn'
 FUNCTION s3d_scene_get_primitive(scn, iprim, prim)
  USE ISO_C_BINDING, ONLY: C_INT, C_PTR
  IMPLICIT NONE
  TYPE(C_PTR), INTENT(IN), VALUE :: scn
  INTEGER(C_INT), INTENT(IN), VALUE :: iprim ! in [1, #prims]; See (1)
  TYPE(S3D_PRIMITIVE), INTENT(OUT) :: prim
  INTEGER(C_INT) :: s3d_scene_get_primitive
  s3d_scene_get_primitive = s3d_scene_get_primitive_0(scn, iprim-1, prim)
 END FUNCTION s3d_scene_get_primitive

END MODULE S3D_BINDING

