/* Copyright (C) |Meso|Star> 2015-2016 (contact@meso-star.com)
 *
 * This software is a collection of files whose purpose is to give access
 * to the Star-3D library from Fortran programs
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms. */

#ifndef S3D_COMPLEMENTS_H
#define S3D_COMPLEMENTS_H

#include "star/s3d.h"

/* Library symbol management */
#if defined(F3D_SHARED_BUILD) /* Build shared library */
  #define F3D_API extern EXPORT_SYM
#elif defined(F3D_STATIC) /* Use/build static library */
  #define F3D_API extern LOCAL_SYM
#else /* Use shared library */
  #define F3D_API extern IMPORT_SYM
#endif

F3D_API const struct s3d_primitive s3d_primitive_null;

/* Invalid vertex data */
F3D_API const struct s3d_vertex_data s3d_vertex_data_null;

/* Constant defining a NULL intersection. Should be used to initialize a hit */
F3D_API const struct s3d_hit s3d_hit_null;

#endif
