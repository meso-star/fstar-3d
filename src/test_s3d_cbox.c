/* Copyright (C) |Meso|Star> 2015 (contact@meso-star.com)
 *
 * This software is a collection of files whose purpose is to give access
 * to the Star-3D library from Fortran programs
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms. */

#include "test_s3d_cbox.h"

#include <rsys/rsys.h>
#include <stdint.h>

/*******************************************************************************
 * Box
 ******************************************************************************/
const float cbox_walls[] = {
  552.f, 0.f,   0.f,
  0.f,   0.f,   0.f,
  0.f,   559.f, 0.f,
  552.f, 559.f, 0.f,
  552.f, 0.f,   548.f,
  0.f,   0.f,   548.f,
  0.f,   559.f, 548.f,
  552.f, 559.f, 548.f
};

const unsigned cbox_walls_nverts = sizeof(cbox_walls) / sizeof(float[3]);

const unsigned cbox_walls_ids[] = {
  0, 1, 2, 2, 3, 0, /* Bottom */
  4, 5, 6, 6, 7, 4, /* Top */
  1, 2, 6, 6, 5, 1, /* Left */
  0, 3, 7, 7, 4, 0, /* Right */
  2, 3, 7, 7, 6, 2  /* Back */
};
const unsigned cbox_walls_ntris = sizeof(cbox_walls_ids) / sizeof(unsigned[3]);

const struct cbox_desc cbox_walls_desc = { cbox_walls, cbox_walls_ids };

/*******************************************************************************
 * Short/tall blocks
 ******************************************************************************/
const float cbox_short_block[] = {
  130.f, 65.f,  0.f,
  82.f,  225.f, 0.f,
  240.f, 272.f, 0.f,
  290.f, 114.f, 0.f,
  130.f, 65.f,  165.f,
  82.f,  225.f, 165.f,
  240.f, 272.f, 165.f,
  290.f, 114.f, 165.f
};

const float cbox_tall_block[] = {
  423.0f, 247.0f, 0.f,
  265.0f, 296.0f, 0.f,
  314.0f, 456.0f, 0.f,
  472.0f, 406.0f, 0.f,
  423.0f, 247.0f, 330.f,
  265.0f, 296.0f, 330.f,
  314.0f, 456.0f, 330.f,
  472.0f, 406.0f, 330.f
};

const unsigned cbox_block_ids[] = {
  4, 5, 6, 6, 7, 4,
  1, 2, 6, 6, 5, 1,
  0, 3, 7, 7, 4, 0,
  2, 3, 7, 7, 6, 2,
  0, 1, 5, 5, 4, 0
};

const unsigned cbox_block_nverts = sizeof(cbox_short_block) / sizeof(float[3]);
const unsigned cbox_block_ntris = sizeof(cbox_block_ids) / sizeof(unsigned[3]);

/*******************************************************************************
 * Callbacks
 ******************************************************************************/
void
cbox_get_ids(const unsigned itri, unsigned ids[3], void* data)
{
  const unsigned id = itri * 3;
  struct cbox_desc* desc = data;
  NCHECK(desc, NULL);
  ids[0] = desc->indices[id + 0];
  ids[1] = desc->indices[id + 1];
  ids[2] = desc->indices[id + 2];
}

void
cbox_get_position(const unsigned ivert, float position[3], void* data)
{
  struct cbox_desc* desc = data;
  NCHECK(desc, NULL);
  position[0] = desc->vertices[ivert*3 + 0];
  position[1] = desc->vertices[ivert*3 + 1];
  position[2] = desc->vertices[ivert*3 + 2];
}

void
cbox_get_normal(const unsigned ivert, float normal[3], void* data)
{
  (void)ivert, (void)data;
  normal[0] = 1.f;
  normal[1] = 0.f;
  normal[2] = 0.f;
}

void
cbox_get_uv(const unsigned ivert, float uv[2], void* data)
{
  (void)ivert, (void)data;
  uv[0] = -1.f;
  uv[1] = 1.f;
}


