! Copyright (C) |Meso|Star> 2015 (contact@meso-star.com)
!
! This software is a collection of files whose purpose is to give access
! to the Star-3D library from Fortran programs
!
! This software is governed by the CeCILL license under French law and
! abiding by the rules of distribution of free software. You can use,
! modify and/or redistribute the software under the terms of the CeCILL
! license as circulated by CEA, CNRS and INRIA at the following URL
! "http://www.cecill.info".
!
! As a counterpart to the access to the source code and rights to copy,
! modify and redistribute granted by the license, users are provided only
! with a limited warranty and the software's author, the holder of the
! economic rights, and the successive licensors have only limited
! liability.
!
! In this respect, the user's attention is drawn to the risks associated
! with loading, using, modifying and/or developing or reproducing the
! software by the user in light of its specific status of free software,
! that may mean that it is complicated to manipulate, and that also
! therefore means that it is reserved for developers and experienced
! professionals having in-depth computer knowledge. Users are therefore
! encouraged to load and test the software's suitability as regards their
! requirements in conditions enabling the security of their systems and/or
! data to be ensured and, more generally, to use and operate it in the
! same conditions as regards security.
!
! The fact that you are presently reading this means that you have had
! knowledge of the CeCILL license and that you accept its terms.

#ifndef S3D_BINDING_INC
#define S3D_BINDING_INC

! Syntactic sugar use during the setup of the shape. Setting a vertex data
! functor to S3D_KEEP means that this vertex data will not be updated
#define S3D_KEEP C_NULL_PTR

! Value of an invalid identifer
#define S3D_INVALID_ID (Huge(INT(0, C_INT)))

! Helper macro that defines whether or not 2 primites are equal
#define S3D_PRIMITIVE_EQ(Prim0, Prim1)  ( (Prim0%prim_id .EQ. Prim1%prim_id) .AND. (Prim0%geom_id .EQ. Prim1%geom_id) .AND. (Prim0%inst_id .EQ. Prim1%inst_id) )

! Helper macro that defines whether or not the hit is valid, i.e. the ray
! intersects a shape or not
#define S3D_HIT_NONE(Hit) (Hit%distance .GE. HUGE(0.))

#endif
