/* Copyright (C) |Meso|Star> 2015-2016 (contact@meso-star.com)
 *
 * This software is a collection of files whose purpose is to give access
 * to the Star-3D library from Fortran programs
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms. */

#ifndef TEST_S3D_CBOX_H
#define TEST_S3D_CBOX_H

#include <star/s3d.h>

/* Library symbol management */
#if defined(F3DT_SHARED_BUILD) /* Build shared library */
  #define F3DT_API extern EXPORT_SYM
#elif defined(S3D_STATIC) /* Use/build static library */
  #define F3DT_API extern LOCAL_SYM
#else /* Use shared library */
  #define F3DT_API extern IMPORT_SYM
#endif

struct cbox_desc{
  const float* vertices;
  const unsigned* indices;
};

BEGIN_DECLS

F3DT_API const float cbox_walls[];
F3DT_API const unsigned cbox_walls_ids[];
F3DT_API const float cbox_short_block[];
F3DT_API const float cbox_tall_block[];
F3DT_API const unsigned cbox_block_ids[];

F3DT_API const unsigned cbox_walls_nverts;
F3DT_API const unsigned cbox_walls_ntris;
F3DT_API const unsigned cbox_block_nverts;
F3DT_API const unsigned cbox_block_ntris;
F3DT_API const struct cbox_desc cbox_walls_desc;

/*******************************************************************************
 * Callbacks
 ******************************************************************************/
F3DT_API void
cbox_get_ids(const unsigned itri, unsigned ids[3], void* data);

F3DT_API void
cbox_get_position(const unsigned ivert, float position[3], void* data);

F3DT_API void
cbox_get_normal(const unsigned ivert, float normal[3], void* data);

F3DT_API void
cbox_get_uv(const unsigned ivert, float uv[2], void* data);

END_DECLS

#endif /* TEST_S3D_CBOX_H */

